const CardHeader = () => {
  return (
    <div className="Card__header">
      header
    </div>
  )
}

const CardBody = (props) => {
  return (
    <div className="Card__body">
      {props.children}
    </div>
  )
}

const Card = (props) => {
  console.log(props)
  return (
    <div className="Card">
      <CardHeader />
      <CardBody>{props.children}</CardBody>
    </div>
  )
}

export default Card;
export { CardHeader, CardBody };
