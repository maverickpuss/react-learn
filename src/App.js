import React, { Component } from 'react';
import Table from "./Table";
import Card from "./Card";

class App extends Component {
  render() {
    const heading = <h1 className="blabla">hello, worl43d!</h1>;
    const bla = () => Math.random()

    return (
      <div className="App">
        {heading}
        <p>{bla()}</p>
        <Table />
        <hr />
        <Card>
          <span>yep, I'm the special `children` props</span>
        </Card>
      </div>
    );
  }
}

export default App;
