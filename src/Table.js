import React, { Component } from 'react';
import AddCharacter from "./AddCharacter";

class TableBody extends Component {
  render() {
    const { characterData, removeCharacter } = this.props;

    const rows = characterData.map(({name, job}, idx) => {
      return (
        <tr key={idx}>
          <td>{name}</td>
          <td>{job}</td>
          <td>
            <button onClick={() => removeCharacter(idx)}>remove</button>
          </td>
        </tr>
      );
    });


    return <tbody>{rows}</tbody>;
  }
}

class Table extends Component {
  state = {
    characters: [
      {
        name: 'Charlie',
        job: 'Janitor',
      },
      {
        name: 'Mac',
        job: 'Bouncer',
      },
      {
        name: 'Dee',
        job: 'Aspring actress',
      },
      {
        name: 'Dennis',
        job: 'Bartender',
      },
    ]
  }

  addCharacter = (data) => {
    this.setState({
      characters: [...this.state.characters, {...data}]
    });
  }

  removeCharacter = (index) => {
    this.setState({
      characters: this.state.characters.filter((_, i) => {
        return i !== index;
      })
    })
  }

  render() {
    return (
      <div>
        <table className="Table">
          <thead>
            <tr>
              <th>name</th>
              <th>job</th>
              <th>actions</th>
            </tr>
          </thead>

          <TableBody
            characterData={this.state.characters}
            removeCharacter={this.removeCharacter}
          />
        </table>

        <AddCharacter addCharacter={this.addCharacter} />
      </div>
    );
  }
}

export default Table;
