import React, { Component } from 'react';

class AddCharacter extends Component {
  initState = {
    name: "",
    job: ""
  }

  state = {
    ...this.initState
  }

  handleChange = (job, name) => {
    this.setState({
      [name]: job
    });
  }

  submit = () => {
    this.props.addCharacter(this.state)
    this.setState(this.initState)
  }

  // lifecycle hook
  componentDidMount() {
    console.log("mounted")
  }

  render() {
    return (
      <div className="AddCharacter">
        <div>
          <label>name:</label>
          <input
            type="text"
            value={this.state.name}
            onChange={(evt) => this.handleChange(evt.target.value, "name")}
          />
        </div>

        <div>
          <label>job:</label>
          <input
            type="text"
            value={this.state.job}
            onChange={(evt) => this.handleChange(evt.target.value, "job")}
         />
        </div>

        <div>
          <button onClick={this.submit}>
            Add
          </button>
        </div>
      </div>
    );
  }
}

export default AddCharacter;
